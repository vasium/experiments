using UnityEngine;

public class RayCastBasedTagSelector : MonoBehaviour, ISelector
{
    [SerializeField] private string selectableTag = "Selectable";
    private Transform selection;

    public void Check(Ray ray)
    {
        selection = null;
        if (Physics.Raycast(ray, out var hit))
        {
            var hitTransform = hit.transform;
            if (hitTransform.CompareTag(selectableTag))
            {
                selection = hitTransform;
            }
        }
    }

    public Transform GetSelection()
    {
        return selection;
    }
}